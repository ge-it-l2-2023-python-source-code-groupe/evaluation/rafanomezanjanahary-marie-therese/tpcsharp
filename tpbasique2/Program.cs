﻿using System;
namespace tpbasique2;

class TPBasique2
{
    public static void MainTPBasique2()
    {
        bool a;
        double abs,ord,rayon = 0;
        while(true){
            Console.Write("Donner l'abscisse du centre:");
            if(double.TryParse(Console.ReadLine(),out double abs1)){
                abs = abs1;
                break;
            }else{
                Console.WriteLine("saisi invalide!!Reesseyer");
            }
        }
        while(true){
            Console.Write("Donner l'ordonnée du centre:");
            if(double.TryParse(Console.ReadLine(),out double ord1)){
                ord = ord1;
                break;
            }else{
                Console.WriteLine("saisi invalide!!Reesseyer");
            }
        }
        while(true){
            Console.Write("Donner le rayon:");
            if(double.TryParse(Console.ReadLine(),out double rayon1)){
                rayon = rayon1;
                break;
            }else{
                Console.WriteLine("saisi invalide!!Reesseyer");
            }
        }
        Console.WriteLine("");
        Point MonPoint = new Point(abs,ord);
        Cercle monCercle = new Cercle(MonPoint,rayon);
        monCercle.Display();
        Console.WriteLine($"Le périmètre est : {monCercle.GetPerimeter():F2}");
        Console.WriteLine($"La surface est : {monCercle.GetSurface():F2}");

        Console.WriteLine("");
        double x,y = 0;
        while(true){
            Console.WriteLine("Donner un point;");
            Console.Write("X : ");  
            if(double.TryParse(Console.ReadLine(),out double x1)){
                x = x1;
                break;
            }else{
                Console.WriteLine("saisi invalide!!Reesseyer");
            }
        }
        while(true){
            Console.Write("Y : ");
            if(double.TryParse(Console.ReadLine(),out double y1)){
                y = y1;
                break;
            }else{
                Console.WriteLine("saisi invalide!!Reesseyer");
            }
        }
        Console.WriteLine("");
        Point MonPoint2 = new Point(x,y);
        MonPoint2.Display();
        monCercle.IsInclude(MonPoint2);
    }
}


public class Point{
    
    //definition des attributs X et Y
    public double X{get;set;}
    public double Y{get;set;}
   
   //definition de constructeur
    public Point(double monX,double monY){
        X = monX;
        Y = monY;
    }
    //methodes
    public void Display(){
        Console.WriteLine($"POINT({X},{Y})");
    }
}

public class Cercle{
    private double pi = Math.PI;

    public Point MonPoint {get;set;}
    
    public double Rayon {get;set;}

    public Cercle(Point userPoint,double userRayon){
        MonPoint = userPoint;
        Rayon = userRayon;
    } 

    public double GetPerimeter(){
        return pi *2*Rayon; 
    }

    public double GetSurface(){
        return pi*Rayon*Rayon;
    }

    public void IsInclude(Point p){
        double distance = Math.Sqrt((p.X - MonPoint.X) * (p.X - MonPoint.X) + (p.Y - MonPoint.Y) * (p.Y - MonPoint.Y));
        if(distance <= Rayon){
            Console.WriteLine("Le point appartient au cercle.");
        } else{
            Console.WriteLine("Le point n'appartient pas au cercle.");
        }
    }

    public void Display(){
        Console.WriteLine($"CERCLE({MonPoint.X},{MonPoint.Y},{Rayon})");
    }

    
}

