class Alarm {
    public string AlarmName { get; set; }
    public TimeOnly AlarmTime { get; set; }
    public List<string> AlarmDaysString { get; set; }
    public List<DayOfWeek> AlarmDays { get; set; }
    public bool IsRepeted { get; set; }

    public Alarm() {
        AlarmName = "";
        AlarmTime = new TimeOnly();
        AlarmDaysString = new List<string> {};
        AlarmDays = new List<DayOfWeek> {};
        IsRepeted = false;
    }

    public Alarm(string name, TimeOnly time, string daysString, bool isRepeted) : this() {
        AlarmName = name;
        AlarmTime = time;
        AlarmDaysString = ConvertToStringDays(daysString);
        AlarmDays = ConvertToDays(daysString);
        IsRepeted = isRepeted;
    }

    private List<DayOfWeek> ConvertToDays(string daysString) {
        List<DayOfWeek> dayList = new List<DayOfWeek> {};
        string dayString;
        for (int i = 0; i < daysString.Length; i++) {
            dayString = "";
            if (daysString[i] != ' ') {
                while (i < daysString.Length && daysString[i] != ' ') {
                    dayString += daysString[i];
                    i++;
                }
                switch (dayString.ToLower()) {
                    case "l":
                        dayList.Add(DayOfWeek.Monday);
                        break;
                    case "m":
                        dayList.Add(DayOfWeek.Tuesday);
                        break;
                    case "me":
                        dayList.Add(DayOfWeek.Wednesday);
                        break;
                    case "j":
                        dayList.Add(DayOfWeek.Thursday);
                        break;
                    case "v":
                        dayList.Add(DayOfWeek.Friday);
                        break;
                    case "s":
                        dayList.Add(DayOfWeek.Saturday);
                        break;
                    case "d":
                        dayList.Add(DayOfWeek.Sunday);
                        break;
                    default:
                        break;
                }
            }
        }
        return dayList;
    }

    private List<string> ConvertToStringDays(string daysString) {
        List<string> dayList = new List<string> {};
        string dayString;
        for (int i = 0; i < daysString.Length; i++) {
            dayString = "";
            if (daysString[i] != ' ') {
                while (i < daysString.Length && daysString[i] != ' ') {
                    dayString += daysString[i];
                    i++;
                }
                switch (dayString.ToLower()) {
                    case "l":
                        dayList.Add("Lun.");
                        break;
                    case "m":
                        dayList.Add("Mar.");
                        break;
                    case "me":
                        dayList.Add("Mer.");
                        break;
                    case "j":
                        dayList.Add("Jeu.");
                        break;
                    case "v":
                        dayList.Add("Ven.");
                        break;
                    case "s":
                        dayList.Add("Sam.");
                        break;
                    case "d":
                        dayList.Add("Dim.");
                        break;
                    default:
                        break;
                }
            }
        }
        return dayList;
    }

    public string FormatAlarmDays() {
        string aDFormated = "";
        foreach(string day in AlarmDaysString) {
            aDFormated += day.ToString() + ", ";
        }
        string aDFormatedCorrect = "";
        for (int i = 0; i < aDFormated.Length - 2; i++) {
            aDFormatedCorrect += aDFormated[i];
        }
        return aDFormatedCorrect;
    }

    public DateTime NextDateOfDay(DayOfWeek day) {
        DateTime dateTime = DateTime.Today;
        Console.WriteLine(day + ";" + dateTime.DayOfWeek);
        while (dateTime.DayOfWeek != day) {
            dateTime = dateTime.AddDays(1);
        }
        return dateTime;
    }

    public string FormatAlarm() {
        if (IsRepeted) {
            return $@"{AlarmName} - {AlarmTime} (Périodique)
{FormatAlarmDays()}";
        }
        return $@"{AlarmName} - {AlarmTime}
{NextDateOfDay(AlarmDays[0]).ToLongDateString()}";
    }
}