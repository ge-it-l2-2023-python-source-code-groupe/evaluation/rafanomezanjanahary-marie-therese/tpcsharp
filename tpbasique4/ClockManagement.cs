using ConsoleManaging;
namespace ClockManaging;

static class ClockManagement {
    public static DateTime Date { get; set; }
    public static readonly IList<TimeZoneInfo> TimeZoneList = TimeZoneInfo.GetSystemTimeZones();
    public static readonly string LocalTimeZoneId = TimeZoneInfo.Local.Id;
    public static readonly string LocalTimeZoneDisplayName = TimeZoneInfo.Local.DisplayName;
    public static readonly int LocalTimeZoneHourOffset = TimeZoneInfo.Local.BaseUtcOffset.Hours;
    public static readonly int LocalTimeZoneMinuteOffset = TimeZoneInfo.Local.BaseUtcOffset.Minutes;
    public static List<TimeZoneInfo> TZToDisplay = new List<TimeZoneInfo> {};

    public static void ShowTimeZones() {
        foreach (TimeZoneInfo timeZone in TimeZoneList) {
            Console.WriteLine($"ID : {timeZone.Id}");
            Console.WriteLine($"Name : {timeZone.DisplayName}");
            Console.WriteLine("------------------------");
        }
    }

    public static void ShowTimeZoneClock(TimeZoneInfo timeZone) {
        while (!Console.KeyAvailable) {
            Console.WriteLine(TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone));
            Thread.Sleep(1000);
            
            if (!Console.KeyAvailable)
                ConsoleManagement.DeleteLinesfromHere(1);
        }

        // Efface le caractère tapé quand une touche est pressée pour sortir de l'horloge.
        Console.ReadKey();
        Console.WriteLine("");
        ConsoleManagement.DeleteLinesfromHere(1);
    }

    public static List<TimeZoneInfo> SearchSuggestion(string searchString) {
        List<TimeZoneInfo> correspondence = new List<TimeZoneInfo> {};
        foreach (TimeZoneInfo element in ClockManagement.TimeZoneList) {
            if (element.DisplayName.ToLower().Contains(searchString.ToLower())) {
                correspondence.Add(element);
            }
        }
        return correspondence;
    }
}