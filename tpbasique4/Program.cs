﻿using ClockManaging;
using ConsoleManaging;

namespace tpbasique4;

class TPBasique4 {
    public static void MainTPBasique4() {
        Task task1;
        CancellationTokenSource cts;
        CancellationTokenSource cts2;
        List<Task> DisplayTZTask = new List<Task> {};
        List<TimeZoneInfo> suggestions;
        List<Alarm> alarmList = new List<Alarm> {};

        Console.WriteLine(@" Bonjour!   Bienvennue à notre Horloge");

        while (true) {
            Console.Write(@"
Vous etes dans le menu principale .
Choisissez une option : 
1 - Alarme 
2 - Horloge 
3 - Quitter 

Quel est votre choix ? ");

            string choice = Console.ReadLine() ?? "";

            while (choice == "1") {
                Console.Write(@"
Choisissez une option : 
1 - Voir les Alarmes actifs
2 - Creer une alarme 
3 - Retour menu principale

Quel est votre choix ? ");

                string alarmChoice = Console.ReadLine() ?? "";

                if (alarmChoice == "1") {
                    if (alarmList.Count == 0) {
                        Console.WriteLine("Il n'y a pas d'alarme actif.");
                        Console.ReadKey();
                    } else {
                        foreach(Alarm alarm in alarmList) {
                            Console.WriteLine(alarm.FormatAlarm());
                        }
                        Console.ReadKey();
                    }
                } else if (alarmChoice == "2") {
                    Console.WriteLine("Infos de la nouvelle alarme :");
                    Console.Write("Nom de l'alarme : ");
                    string alarmName = Console.ReadLine() ?? "Réveil";
                    Console.Write("Heure de l'alarme (hh:mm) : ");
                    bool a = TimeOnly.TryParse(Console.ReadLine(), out TimeOnly alarmTime);
                    string toRepeat = "";
                    while (!toRepeat.ToLower().Equals("o") && !toRepeat.ToLower().Equals("n")) {
                        Console.Write("Répéter l'alarme ? (O/n) ");
                        toRepeat = Console.ReadLine() ?? "n";
                    }
                    bool isRepeted;
                    if (toRepeat.ToLower()[0] == 'o') {
                        isRepeted = true;
                    } else {
                        isRepeted = false;
                    }

                    string alarmDays;

                    if (isRepeted) {
                        Console.Write("Jours de répétition (L/M/ME/J/V/S/D) : ");
                        alarmDays = Console.ReadLine() ?? "L";
                    } else {
                        Console.Write("Jours de l'alarme (L/M/ME/J/V/S/D) : ");
                        alarmDays = Console.ReadLine() ?? "L";
                    }

                    Alarm myAlarm = new Alarm(alarmName, alarmTime, alarmDays, isRepeted);
                    foreach (string day in myAlarm.AlarmDaysString)
                        Console.WriteLine(day);

                    alarmList.Add(myAlarm);
                } else if (alarmChoice == "3") {
                    break;
                }
            }

            while (choice == "2") {
                Console.WriteLine($"Heure actuelle ({ClockManagement.LocalTimeZoneId}).\n\n");

                Console.Write(@"
Choisissez une option : 
1 - Voir les Horloges actifs
2 - Ajouter une horloge 
3 - Retour menu principale

Quel est votre choix ? ");
                cts = new CancellationTokenSource();

                task1 = Task.Run(() => {
                    while (!cts.IsCancellationRequested) {
                        ConsoleManagement.SetLineAtFromHere(DateTime.Now.ToLongTimeString(), 7);

                        Thread.Sleep(1000);
                    }
                }, cts.Token);

                ExecuteTask(task1);
                string clockChoice = Console.ReadLine() ?? "";
                cts.Cancel();
                cts.Dispose();

                if (clockChoice == "1" && ClockManagement.TZToDisplay.Count > 0) {
                    cts2 = new CancellationTokenSource();

                    task1 = Task.Run(() => {
                        foreach (TimeZoneInfo timeZone in ClockManagement.TZToDisplay) {
                            Console.WriteLine("");
                            Console.WriteLine("");
                        }
                        while (!cts2.IsCancellationRequested) {
                            int i = 0;
                            foreach (TimeZoneInfo timeZone in ClockManagement.TZToDisplay) {
                                ConsoleManagement.SetLineAtFromHere(timeZone.DisplayName, 2*(ClockManagement.TZToDisplay.Count - i));
                                ConsoleManagement.SetLineAtFromHere(TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone).ToLongTimeString(), 
                                2*(ClockManagement.TZToDisplay.Count - i) - 1);
                                i++;
                            }
                            
                            Thread.Sleep(1000);
                        }
                    }, cts2.Token);

                    ExecuteTask(task1);

                    Console.ReadKey();
                    cts2.Cancel();
                    cts2.Dispose();
                } else if (clockChoice == "1") {
                    Console.WriteLine("Il n'y a pas d'horloge à afficher.");
                    Console.ReadKey();
                } else if (clockChoice == "2") {
                    while (true) {
                        Console.WriteLine("Entrez votre recherche (Suggestions possibles, tapez tout de suite Entrer): ");
                        string searchString = Console.ReadLine() ?? "";

                        suggestions = ClockManagement.SearchSuggestion(searchString);

                        if(suggestions.Count > 1) {
                            Console.WriteLine("Suggestions : ");
                            foreach (TimeZoneInfo suggestion in suggestions)
                            {
                                Console.WriteLine(suggestion.DisplayName);
                            }
                        } else if (suggestions.Count == 0) {
                            Console.WriteLine("Aucune suggestion trouvée.");
                        } else if (!ClockManagement.TZToDisplay.Contains(suggestions[0])) {
                            Console.WriteLine(suggestions[0].DisplayName);
                            ClockManagement.TZToDisplay.Add(suggestions[0]);
                            break;
                        } else {
                            Console.WriteLine("L'horloge est déjà actif.");
                        }
                    }
                } else if (clockChoice == "3") {
                    break;
                }
            }

            if (choice == "3") {
                break;
            }
        }
    }

    static async void ExecuteTask(Task task) {
        try {
            await task;
        } catch(TaskCanceledException e) {
            e.GetBaseException();
        }
    }

}
