using ClockManaging;

namespace ConsoleManaging;

static class ConsoleManagement {
    public static void DeleteLinesfromHere(int numOfLine) {
        // Récupérer la position actuelle du curseur
        int cursorTop = Console.CursorTop;

        // Définir la position du curseur à la ligne spécifiée
        Console.SetCursorPosition(0, cursorTop - numOfLine);

        // Écrire une ligne vide pour effacer le contenu de la ligne
        for(int i=0;i<numOfLine;i++) {
            Console.Write(new string(' ', Console.WindowWidth));
        }

        Console.SetCursorPosition(0, cursorTop - numOfLine);
    }

    public static void SetLineAtFromHere(string setString, int numOfLine) {
        int CursorLeft = Console.CursorLeft;
        int cursorTop = Console.CursorTop;
        if (CursorLeft < 0) CursorLeft = 0;
        if (cursorTop < 0) cursorTop = 0;
        Console.SetCursorPosition(0, cursorTop - numOfLine);

        Console.Write(setString);

        Console.SetCursorPosition(CursorLeft, cursorTop);
    }

}