﻿using GestionJoueurs;
using MancheJeu;

namespace tpbasique3;
class TPBasique3
{
    public static void MainTPBasique3()
    {
        String listeManches = "";
        Joueurs.listeJoueurs = "";
        Joueurs.pointsJoueurs = "";
        Joueurs.nbJoueurs = 0;


        Console.Write("Entrez le nombre de joueurs : ");
        Boolean a = Int32.TryParse(Console.ReadLine(), out Joueurs.nbJoueurs);

        Int32 num = 1;
        while(true) {
            Console.Write($"Entrez le nom du joueur n° {num} : ");
            String nom = Console.ReadLine() ?? "";

            Joueurs.AjouterJoueur(nom);

            if(num == Joueurs.nbJoueurs) {
                break;
            }
            num++;
        }

        while(true) {
            Console.WriteLine("\nScores actuels : ");
            Console.Write(Joueurs.FormaterScore());
            Console.ReadKey();
            Console.Write($@"
Choisissez une option : 
1 - Nouvelle manche 
2 - Voir l'historique des manches 
3 - Quitter 

Quel est votre choix ? ");

            String choix = Console.ReadLine() ?? "";

            if(choix.Equals("1")) {
                Manche thisManche = new Manche(Joueurs.nbJoueurs, Joueurs.listeJoueurs);
                thisManche.DateHeure = DateTime.Now.ToString();

                num = 1;
                String resultat = "";
                while(true) {
                    while(true) {
                        Console.Write($"\nTour de {Joueurs.GetNomJoueur(num-1)} -- Voulez-vous lancer le dé ? (y/n) ");
                        choix = Console.ReadLine() ?? "";


                        if(choix.Equals("y")) {
                            resultat += Joueurs.Jouer() + ",";
                            break;
                        } else if(choix.Equals("n")) {
                            Console.WriteLine("Vous avez passé votre tour!");
                            resultat += 0 + ",";
                            break;
                        }else{
                            Console.WriteLine("saisie invalide!! tapez 'y' si oui ,tapez 'n' si non:");
                            continue;
                        } 
                    }

                    if(num == Joueurs.nbJoueurs) {
                        break;
                    }
                    num++;
                }

                thisManche.SetPointsJoueurs(resultat);
                Joueurs.SetScoresJoueurs(thisManche.PointsJoueurs);
                thisManche.EtatScores = Joueurs.FormaterScore();

                listeManches += thisManche.InfoManche() + "\n";
            } else if(choix.Equals("2")) {
                Console.WriteLine(listeManches);
            } else if(choix.Equals("3")) {
                break;
            }
        }
    }
}
