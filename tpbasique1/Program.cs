﻿using clients;
using comptes;
namespace tpbasique1
{
    

    class TPBasique1
    {
        

        public static void MainTPBasique1()
        {
    #pragma warning disable CS8632 
            string ? cin, firstName, lastName, tel;
    #pragma warning restore CS8632 
            double somme=default;

            // #Create Compte Client 1
            
            Console.Write($"Compte 1:\nEntrer Le CIN: ");
            cin = Console.ReadLine();
            Console.Write($"Entrer Le Nom: ");
            firstName = Console.ReadLine();
            Console.Write($"Entrer Le Prénom: ");
            lastName = Console.ReadLine();
            Console.Write($"Entrer Le numéro de télephone: ");
            tel = Console.ReadLine();

            Comptes cpt1 = new Comptes(new Clients(cin, firstName, lastName, tel));
            Console.WriteLine($"Détails du compte:{cpt1.Resumer()}");
            
        
            // # Crediter & Debiter Compte Client 1
            
            // Crediter cpt 1
            while (true){
                Console.Write($"Entrer le montant à déposer: ");
                if(double.TryParse(Console.ReadLine(),out double montant)){
                    somme=montant;
                    break;
                }else{
                    Console.WriteLine("Saisi invalide!! Veuillez entrez un montant.");
                }
            } 

            cpt1.Crediter((float)somme);
            Console.WriteLine($"Opération bien effectuée{cpt1.Resumer()}");
            //Debiter cpt 1
            while (true){
                Console.Write($"Entrer le montant à retirer: ");
                if(double.TryParse(Console.ReadLine(),out double montant)){
                    somme = montant;
                    break;
                }else{
                    Console.WriteLine("Saisi invalide!! Veuillez entrez un montant.");
                }
            }
            cpt1.Debiter((float)somme);
            Console.WriteLine($"Opération bien effectuée{cpt1.Resumer()}");
            
        
            // # Create Compte Client 2

            Console.Write($"\n\n\nCompte 2:\nEntrer Le CIN: ");
            cin = Console.ReadLine();
            Console.Write($"Entrer Le Nom: ");
            firstName = Console.ReadLine();
            Console.Write($"Entrer Le Prénom: ");
            lastName = Console.ReadLine();
            Console.Write($"Entrer Le numéro de télephone: ");
            tel = Console.ReadLine();

            Comptes cpt2 = new Comptes(new Clients(cin, firstName, lastName, tel));
            Console.WriteLine($"Détails du compte:{cpt2.Resumer()}");

        
        

            //Crediter cpt2 -> cpt1
            Console.WriteLine($"Crediter le compte {cpt2.Code} à partir du compte {cpt1.Code}");
            while(true){
                Console.Write($"Entrer le montant à déposer: ");
                if(double.TryParse(Console.ReadLine(),out double montant)){
                    somme = montant;
                    break;
                }else{
                    Console.WriteLine("Saisi invalide!! Veuillez entrez un montant.");
                }
            }
            cpt2.Crediter((float)somme, cpt1);
            Console.WriteLine($"Opération bien effectuée");
            //Debiter cpt1 -> cpt2
            Console.WriteLine($"Débiter le compte {cpt1.Code} et créditer le compte {cpt2.Code}");
            while(true){
                Console.Write($"Entrer le montant à retirer: ");
                if(double.TryParse(Console.ReadLine(),out double montant)){
                    somme = montant;
                    break;
                }else{
                    Console.WriteLine("Saisi invalide!! Veuillez entrez un montant.");
                }
            }
            cpt1.Debiter((float)somme, cpt2);
            Console.Write($"Opération bien effectuée");

    

            Console.WriteLine($"{cpt1.Resumer()}{cpt2.Resumer()}");
            Console.Write($"\n\n\n{Comptes.NombreCompteCree()}");
            Console.ReadKey();
        }
    }
}