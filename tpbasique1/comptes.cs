using System;
using clients;

namespace comptes{
    class Comptes
    {
        private float solde;
        public float Solde { get { return solde; } }
        private uint code;
        public uint Code { get { return code; } }
        public Clients Proprietaire { get; set; }

        private static uint compteur = 0;

        public string Resumer()
        {
            return $@"
        ************************
        Numéro de Compte: {Code}
        Solde de compte: {Solde}
        Propriétaire du compte:
        {Proprietaire.Afficher()}
        ************************";
        }

        public static string NombreCompteCree()
        {
            return $"Le nombre de comptes crées: {compteur}";
        }

        public void Debiter(float montant, Comptes compteClient)
        {
            Debiter(montant);
            compteClient.Crediter(montant);
        }

        public void Debiter(float montant)
        {
            solde -= montant;
        }

        public void Crediter(float montant, Comptes compteClient)
        {
            Crediter(montant);
            compteClient.Debiter(montant);
        }

        public void Crediter(float montant)
        {
            solde += montant;
        }

        public Comptes(Clients client, float montant)
        {
            Proprietaire = client;
            solde = montant;
            code = ++compteur;
        }

        public Comptes(Clients client)
        {
            Proprietaire = client;
            solde = default;
            code = ++compteur;
        }
    }
}