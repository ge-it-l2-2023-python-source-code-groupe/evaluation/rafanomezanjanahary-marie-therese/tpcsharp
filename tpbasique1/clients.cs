using System;

namespace clients
{

    class Clients
    {
        public string CIN { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Tel { get; set; }

        public string Afficher()
        {
            return $@"CIN: {CIN}
        NOM: {Nom}
        Prénom: {Prenom}
        Tél : {Tel}";
        }

        public Clients(string cin, string nom, string prenom, string tel)
        {
            CIN = cin.ToUpper();
            Nom = nom;
            Prenom = prenom;
            Tel = tel;
            
        }

        public Clients(string cin, string nom, string prenom)
        {
            CIN = cin.ToUpper();
            Nom = nom;
            Prenom = prenom;
            Tel = "";
        }
    }

    
}